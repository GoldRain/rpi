package com.joseph.TBD.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joseph.TBD.R;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.DetailsListActivity;
import com.joseph.TBD.utils.TouchImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.joseph.TBD.commons.Constants.anatomyImages;
import static com.joseph.TBD.commons.Constants.array_imageLigament;
import static com.joseph.TBD.commons.Constants.array_imageMuscle;
import static com.joseph.TBD.commons.Constants.array_imagePeriosteal1;
import static com.joseph.TBD.commons.Constants.joint_image;
import static com.joseph.TBD.commons.Constants.position;

public class AnatomyFragment extends Fragment {
    DetailsListActivity _activity;
    View view;
    public int imageValue = 0;
    @BindView(R.id.imv_anatomy) TouchImageView imv_anatomy;

    int _index = 0;

    public AnatomyFragment(int index) {
        // Required empty public constructor
        this._index = index;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_anatomy, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        imv_anatomy.setImageResource(anatomyImages.get(_index));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

}

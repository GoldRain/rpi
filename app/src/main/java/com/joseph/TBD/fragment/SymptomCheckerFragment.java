package com.joseph.TBD.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.joseph.TBD.R;
import com.joseph.TBD.adapter.SymptomCheckerListViewAdapter;
import com.joseph.TBD.base.BaseFragment;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.DetailsListActivity;
import com.joseph.TBD.main.MainActivity;
import com.joseph.TBD.main.PainSelcection.PainSelectDetaileActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.joseph.TBD.commons.Constants.KEY_REGION_ARRAY;
import static com.joseph.TBD.commons.Constants.position;
import static com.joseph.TBD.commons.Constants.symptomCheckerList;
import static com.joseph.TBD.commons.Constants.symptom_checker;

public class SymptomCheckerFragment extends BaseFragment {

    MainActivity _activity ;
    View view ;
    @BindView(R.id.lst_symptom_checker) ListView lst_symptom_checker;
    SymptomCheckerListViewAdapter _adapter_symptom;

    @BindView(R.id.edt_search) EditText edt_search;
    @BindView(R.id.imv_cancel) ImageView imv_cancel;
    Context context;

    HashMap<String , Integer> hashPosition = new HashMap<>();

    public SymptomCheckerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_symptom_checker, container, false);
        ButterKnife.bind(this, view);

        loadLayout();

        return view ;
    }

    private void loadLayout() {

        for (int i = 0; i < symptomCheckerList.size(); i++){

            hashPosition.put(symptomCheckerList.get(i), i);
        }

        _adapter_symptom = new SymptomCheckerListViewAdapter(_activity, symptomCheckerList);
        lst_symptom_checker.setAdapter(_adapter_symptom);
        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (edt_search.getText().length()>0){
                    imv_cancel.setVisibility(View.VISIBLE);
                }else {
                    imv_cancel.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                String name = edt_search.getText().toString().toLowerCase(Locale.getDefault());
                _adapter_symptom.filter(name);

            }
        });

        _adapter_symptom.addItem(symptomCheckerList);
        _adapter_symptom.initProducts();
        _adapter_symptom.notifyDataSetChanged();

        lst_symptom_checker.setAdapter(_adapter_symptom);

        lst_symptom_checker.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String name = _adapter_symptom._contents.get(position);

                if(name.length() != 1) {

                    ArrayList<Integer> symptoms = new ArrayList<>();

                    int[] names = symptom_checker[hashPosition.get(name)];
                    for (int z = 0; z < names.length; z++ ){
                        symptoms.add(names[z]);
                    }

                    Intent intent = new Intent(_activity, PainSelectDetaileActivity.class);
                    intent.putExtra(KEY_REGION_ARRAY, symptoms);
                    _activity.overridePendingTransition(0, 0);

                    startActivity(intent);
                }
            }
        });
    }

    @OnClick(R.id.imv_cancel) void removeText(){
        edt_search.setText("");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}

package com.joseph.TBD.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.joseph.TBD.R;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.DetailsListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.joseph.TBD.commons.Constants.array_imageMuscle;
import static com.joseph.TBD.commons.Constants.array_imageLigament;
import static com.joseph.TBD.commons.Constants.array_imagePeriosteal1;
import static com.joseph.TBD.commons.Constants.joint_image;
import static com.joseph.TBD.commons.Constants.nervesImages;
import static com.joseph.TBD.commons.Constants.neuralImages;
import static com.joseph.TBD.commons.Constants.organsImages;
import static com.joseph.TBD.commons.Constants.position;

public class PainSelectionFragment extends Fragment {

    DetailsListActivity _activity;
    View view;
    @BindView(R.id.imv_pain_selection) ImageView ui_imagedetail;

    int _position = 0;
    public PainSelectionFragment(int position) {
        // Required empty public constructor
        this._position = position;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_pain_selection, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        Log.d("painPostion==>", String.valueOf(_position));

        /*Muscle */
        if(Constants.select_Status == 1) {


            ui_imagedetail.setImageResource(array_imageMuscle.get(_position));

            /* Periosteal */
        } else if(Constants.select_Status == 4) {

            ui_imagedetail.setImageResource(array_imagePeriosteal1.get(_position));


            /*Ligament*/
        }else if(Constants.select_Status == 2){

            ui_imagedetail.setImageResource(array_imageLigament.get(_position));

            /*Joint*/
        }else if(Constants.select_Status == 3){
            ui_imagedetail.setImageResource(joint_image.get(_position));
        }

        //nerves
        else if (Constants.select_Status == 5){
            ui_imagedetail.setImageResource(nervesImages.get(_position));
        }

        //Organs
        else if (Constants.select_Status == 6){
            ui_imagedetail.setImageResource(organsImages.get(_position));
        }
        //Neural
        else if (Constants.select_Status == 7){
            ui_imagedetail.setImageResource(neuralImages.get(_position));
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (DetailsListActivity)context;
    }

}

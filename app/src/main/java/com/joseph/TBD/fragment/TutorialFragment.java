package com.joseph.TBD.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.adapter.ImagePreviewAdapter;
import com.joseph.TBD.main.MainActivity;

import java.util.ArrayList;

public class TutorialFragment extends Fragment {

    MainActivity _activity;

    ViewPager ui_viewPager;
    ImagePreviewAdapter _adapter;
    TextView ui_txvImageNo;

    ArrayList<Integer> _imagePaths = new ArrayList<>();
    int _position = 0;

    View view;

    public TutorialFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tutorial, container, false);

 /*       _imagePaths = getIntent().getStringArrayListExtra(Constants.KEY_IMAGEPATH);
        _position = getIntent().getIntExtra(Constants.KEY_POSITION,0);*/


        loadImages();
        loadlayout();
        return view;
    }

    private void loadImages(){

        _imagePaths.add(R.drawable.ic_slide01); _imagePaths.add(R.drawable.ic_slide02); _imagePaths.add(R.drawable.ic_slide03); _imagePaths.add(R.drawable.ic_slide04);_imagePaths.add(R.drawable.ic_slide05);

        _imagePaths.add(R.drawable.ic_slide06);  _imagePaths.add(R.drawable.ic_slide07); _imagePaths.add(R.drawable.ic_slide08);_imagePaths.add(R.drawable.ic_slide09); _imagePaths.add(R.drawable.ic_slide10);

        _imagePaths.add(R.drawable.ic_slide11);  _imagePaths.add(R.drawable.ic_slide12); _imagePaths.add(R.drawable.ic_slide13);_imagePaths.add(R.drawable.ic_slide14);_imagePaths.add(R.drawable.ic_slide15);

        _imagePaths.add(R.drawable.ic_slide16);  _imagePaths.add(R.drawable.ic_slide17); _imagePaths.add(R.drawable.ic_slide18);_imagePaths.add(R.drawable.ic_slide19);_imagePaths.add(R.drawable.ic_slide20);

        _imagePaths.add(R.drawable.ic_slide21); _imagePaths.add(R.drawable.ic_slide22);_imagePaths.add(R.drawable.ic_slide23);_imagePaths.add(R.drawable.ic_slide24);_imagePaths.add(R.drawable.ic_slide25);

        _imagePaths.add(R.drawable.ic_slide26); _imagePaths.add(R.drawable.ic_slide27);_imagePaths.add(R.drawable.ic_slide28);_imagePaths.add(R.drawable.ic_slide29);_imagePaths.add(R.drawable.ic_slide30);

        _imagePaths.add(R.drawable.ic_slide31);_imagePaths.add(R.drawable.ic_slide32);

    }

    private void loadlayout() {

        ui_txvImageNo = (TextView)view.findViewById(R.id.txv_timeline_no);

        ui_viewPager = (ViewPager)view.findViewById(R.id.viewpager);
        _adapter = new ImagePreviewAdapter(_activity);
        ui_viewPager.setAdapter(_adapter);
        _adapter.setDatas(_imagePaths);
        ui_viewPager.setCurrentItem(_position);

        ui_txvImageNo.setText((_position + 1) + " / " + _imagePaths.size());

        ui_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ui_txvImageNo.setText((position + 1) + " / " + _imagePaths.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ImageView imv_back_tutor = (ImageView)view.findViewById(R.id.imv_back_tutor);
        imv_back_tutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ui_viewPager.setCurrentItem(getItem(-1) , true) ;
                //getItem(-1) for back

            }
        });

        ImageView imv_forward_tutor = (ImageView)view.findViewById(R.id.imv_forward_tutor);
        imv_forward_tutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //getItem(+1) for next
                ui_viewPager.setCurrentItem(getItem(+1) , true) ;
            }
        });
    }

    private int getItem(int i) {
        return ui_viewPager.getCurrentItem() + i;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }
}

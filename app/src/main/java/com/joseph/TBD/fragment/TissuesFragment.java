package com.joseph.TBD.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joseph.TBD.R;
import com.joseph.TBD.base.BaseFragment;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.MainActivity;
import com.joseph.TBD.main.SearchByTissureActivity.MuscleActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.joseph.TBD.commons.Constants.KEY_IMAGE_VALUE;
import static com.joseph.TBD.commons.Constants.KEY_STRINGS;
import static com.joseph.TBD.commons.Constants.KEY_STRING_LISTS;
import static com.joseph.TBD.commons.Constants.KEY_TITLE;
import static com.joseph.TBD.commons.Constants.ligamentList;
import static com.joseph.TBD.commons.Constants.jointImageValues;
import static com.joseph.TBD.commons.Constants.jointList;
import static com.joseph.TBD.commons.Constants.jointStrings;
import static com.joseph.TBD.commons.Constants.ligamentImageValues;
import static com.joseph.TBD.commons.Constants.ligamentStrings;
import static com.joseph.TBD.commons.Constants.muscleImageValues;
import static com.joseph.TBD.commons.Constants.muscleList;
import static com.joseph.TBD.commons.Constants.muscleStrings;
import static com.joseph.TBD.commons.Constants.nervesImageValues;
import static com.joseph.TBD.commons.Constants.nervesList;
import static com.joseph.TBD.commons.Constants.nervesStrings;
import static com.joseph.TBD.commons.Constants.neuralImageValues;
import static com.joseph.TBD.commons.Constants.neuralList;
import static com.joseph.TBD.commons.Constants.neuralStrings;
import static com.joseph.TBD.commons.Constants.organsImageValues;
import static com.joseph.TBD.commons.Constants.organsList;
import static com.joseph.TBD.commons.Constants.organsStrings;
import static com.joseph.TBD.commons.Constants.periostealImageValues;
import static com.joseph.TBD.commons.Constants.periostealList;
import static com.joseph.TBD.commons.Constants.periostealStrings;


public class TissuesFragment extends BaseFragment{


    MainActivity _activity ;
    View view ;

    public TissuesFragment(MainActivity activity) {
        this._activity = activity ;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_tissues, container, false);

        ButterKnife.bind(this, view);
        return view ;
    }

    @OnClick(R.id.imv_muscle) void gotoMuscle(){

        gotoListActivity(0);
        Constants.select_Status = 1;
    }

    @OnClick(R.id.imv_lig) void gotoLig(){

        gotoListActivity(1);
        Constants.select_Status = 2;
    }

    @OnClick(R.id.imv_joint) void gotoJoin(){
        gotoListActivity(2);
        Constants.select_Status = 3;
    }

    @OnClick(R.id.imv_periosteal) void gotoPeriosteal(){
        gotoListActivity(3);
        Constants.select_Status = 4;
    }

    @OnClick(R.id.imv_nerves) void gotoNerves(){
        gotoListActivity(4);
        Constants.select_Status = 5;
    }

    @OnClick(R.id.imv_organs) void gotoOrgans(){
       gotoListActivity(5);
        Constants.select_Status = 6;
    }

    @OnClick(R.id.imv_neural_matrix) void gotoNeural(){
       gotoListActivity(6);
        Constants.select_Status = 7;
    }

    void gotoListActivity(int index) {

        String[] strings = {};
        int[] imageValues = {};
        ArrayList<String> stringLists = new ArrayList<>();
        ArrayList<Integer> imageLists = new ArrayList<>();
        String title = "";
        switch (index) {

            //muscle
            case 0:
                strings = muscleStrings;
                imageValues = muscleImageValues;
                stringLists = muscleList;
                title = getString(R.string.muscle);
                break;

            //ligament
            case 1:
                strings = ligamentStrings;
                imageValues = ligamentImageValues;
                stringLists = ligamentList;
                title = getString(R.string.ligament);
                break;

            //joint
            case 2:
                strings = jointStrings;
                imageValues = jointImageValues;
                stringLists = jointList;
                title = getString(R.string.joint);
                break;

            //periosteal
            case 3:
                strings = periostealStrings;
                imageValues = periostealImageValues;
                stringLists = periostealList;
                title = getString(R.string.periosteal);
                break;

            //nerves
            case 4:
                strings = nervesStrings;
                imageValues = nervesImageValues;
                stringLists = nervesList;
                title = getString(R.string.nerves);
                break;

            //organs
            case 5:
                strings = organsStrings;
                imageValues = organsImageValues;
                stringLists = organsList;
                title = getString(R.string.organs);
                break;

            //neural
            case 6:
                strings = neuralStrings;
                imageValues = neuralImageValues;
                stringLists = neuralList;
                title = "Neural Matrix";
                break;
            default:
                break;
        }

        Intent intent = new Intent(_activity, MuscleActivity.class);
        intent.putExtra(KEY_STRINGS, strings);
        intent.putExtra(KEY_IMAGE_VALUE, imageValues);
        intent.putExtra(KEY_STRING_LISTS, stringLists);
        intent.putExtra(KEY_TITLE, title);


        _activity.startActivity(intent);
        _activity.overridePendingTransition(0,0);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}

package com.joseph.TBD.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.joseph.TBD.fragment.AnatomyFragment;
import com.joseph.TBD.fragment.GlossaryFragment;
import com.joseph.TBD.fragment.InformationFragment;
import com.joseph.TBD.fragment.PainSelectionFragment;
import com.joseph.TBD.main.DetailsListActivity;

/**
 * Created by ITWhiz4U on 12/28/2017.
 */

public class DetailsViewPagerAdapter extends FragmentStatePagerAdapter {

    DetailsListActivity _context;

    public DetailsViewPagerAdapter(FragmentManager fm, DetailsListActivity context){
        super(fm);
        this._context = context;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch (position){

            case 0:
                fragment = new AnatomyFragment(_context.index);
                break;

            case 1:
                fragment = new PainSelectionFragment(_context._position);
                break;

            case 2:
                fragment = new InformationFragment(_context._position, _context.name);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){

            case 0:
                title = "Anatomy";
                break;

            case 1:
                title = "Pain Selection";
                break;

            case 2:
                title = "Information";
                break;
        }
        return title;
    }
}

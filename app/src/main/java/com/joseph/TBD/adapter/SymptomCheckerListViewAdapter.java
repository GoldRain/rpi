package com.joseph.TBD.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.main.MainActivity;

import java.util.ArrayList;

/**
 * Created by ITWhiz4U on 1/4/2018.
 */

public class SymptomCheckerListViewAdapter extends BaseAdapter {

    private  static final int TYPE_INDEX = 0;
    private static final int TYPE_USER = 1;

    MainActivity _activity ;

    public ArrayList<String> _contents = new ArrayList<>();
    private ArrayList<String> _allcontents = new ArrayList<>();

    public SymptomCheckerListViewAdapter(MainActivity activity, ArrayList<String> contents){

        this._activity = activity;
        //this._contents = contents;
    }

    @Override
    public int getCount() {
        return _contents.size();
    }

    @Override
    public Object getItem(int position) {
        return _contents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        if (getItem(position).toString().length() > 1)
            return TYPE_USER ;

        return TYPE_INDEX ;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        switch (type) {

            case TYPE_INDEX :{

                IndexHolder indexHolder ;

                if (convertView == null){

                    indexHolder = new IndexHolder();

                    LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.directory_indexlistitem, parent,false);

                    indexHolder.txvIndex = (TextView)convertView.findViewById(R.id.txv_index);

                    convertView.setTag(indexHolder);

                }else {
                    indexHolder = (IndexHolder) convertView.getTag();
                }

                String index = _contents.get(position);
                indexHolder.txvIndex.setText(index);
            }

            break;

            case TYPE_USER: {

                CustomHolder customHolder;

                if (convertView == null) {

                    customHolder = new CustomHolder();

                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.directory_userlistitem, parent, false);

                    customHolder.txvName = (TextView) convertView.findViewById(R.id.txv_content);

                    convertView.setTag(customHolder);

                } else {

                    customHolder = (CustomHolder) convertView.getTag();
                }

                customHolder.txvName.setText(_contents.get(position));
            }

            break;
        }
        return convertView;
    }

    public void addItem(ArrayList<String> entity){

        _allcontents.addAll(entity);
    }

    public void initProducts() {

        _contents.clear();
        _contents.addAll(_allcontents);
    }

    public void filter(String charText) {

        charText = charText.toLowerCase();

        _contents.clear();

        if (charText.length() == 0) {
            _contents.addAll(_allcontents);

        } else {

            for (String product : _allcontents) {

                if (product.toLowerCase().contains(charText))
                {
                    _contents.add(product);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class CustomHolder {

        public TextView txvName;

    }

    public class IndexHolder {

        public TextView txvIndex;
    }
}

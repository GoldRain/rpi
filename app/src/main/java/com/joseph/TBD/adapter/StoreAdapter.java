package com.joseph.TBD.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.model.ItemModel;
import com.joseph.TBD.model.DetailsModel;

import java.util.ArrayList;

import static com.joseph.TBD.commons.Constants.array_imageMuscle;
import static com.joseph.TBD.commons.Constants.array_imageLigament;
import static com.joseph.TBD.commons.Constants.array_imagePeriosteal1;
import static com.joseph.TBD.commons.Constants.ligamentList;
import static com.joseph.TBD.commons.Constants.muscleList;
import static com.joseph.TBD.commons.Constants.nervesImages;
import static com.joseph.TBD.commons.Constants.nervesList;
import static com.joseph.TBD.commons.Constants.neuralImages;
import static com.joseph.TBD.commons.Constants.neuralList;
import static com.joseph.TBD.commons.Constants.organsImages;
import static com.joseph.TBD.commons.Constants.organsList;
import static com.joseph.TBD.commons.Constants.periostealList;
import static com.joseph.TBD.commons.Constants.position;
import static com.joseph.TBD.commons.Constants.screenheight;
import static com.joseph.TBD.commons.Constants.screenwidth;


public class StoreAdapter extends BaseAdapter{

    private ArrayList<ItemModel> _userDatas = new ArrayList<>();
    private ArrayList<ItemModel> _allUserDatas = new ArrayList<>();

    ArrayList<DetailsModel> _painData = new ArrayList<>();
    ArrayList<DetailsModel> _allData = new ArrayList<>();

    private LayoutInflater inflater;

    public StoreAdapter(Context context)
    {
        inflater = LayoutInflater.from(context);

    }

    public void setUserDatas(ArrayList<DetailsModel> images) {

        _allData = images;
        _painData.clear();
        _painData.addAll(_allData);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return _painData.size();
    }

    @Override
    public Object getItem(int i) {
        return _painData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = view;
        ImageView picture;
        TextView name;

        if(v == null){

            v = inflater.inflate(R.layout.store_item, viewGroup, false);
            v.setTag(R.id.picture, v.findViewById(R.id.picture));
            v.setTag(R.id.txv_name, v.findViewById(R.id.txv_name));
        }

        picture = (ImageView) v.getTag(R.id.picture);
        name = (TextView)v.getTag(R.id.txv_name);

        picture.getLayoutParams().height = screenheight / 18*8 ;

        picture.getLayoutParams().width = screenwidth / 2;

        name.getLayoutParams().height = screenheight / 18;
        name.getLayoutParams().width = screenwidth / 2;

        DetailsModel images = (DetailsModel) getItem(i);

        picture.setImageResource(images.get_imageResource());
        name.setText(images.get_name());

        return v;
    }

}

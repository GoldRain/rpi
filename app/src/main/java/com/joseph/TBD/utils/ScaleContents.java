package com.joseph.TBD.utils;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by HugeRain on 2/25/2017.
 */

public class ScaleContents  {

    private void scaleCotents(View rootView, View container){

        // Compute the scaling ratio
        float xScale = (float)container.getWidth() / rootView.getWidth();
        float yScale = (float)container.getHeight() / rootView.getHeight();
        float scale = Math.min(xScale, yScale);

        // Scale our contents
        scaleViewAndChildren(rootView, scale);
    }

    // Scale the given view, its contents, and all of its children by the given factor.

    public static void scaleViewAndChildren(View rootView, float scale) {

        //Retrieve the view's layout information

        ViewGroup.LayoutParams layoutParams = rootView.getLayoutParams();

        //Scale the view itself
        if (layoutParams.width != ViewGroup.LayoutParams.MATCH_PARENT && layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT){

            layoutParams.width *= scale;

        }

        if (layoutParams.height != ViewGroup.LayoutParams.MATCH_PARENT && layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT) {

            layoutParams.height *= scale;
        }

        //if this view has margins, scale those too

        if (layoutParams instanceof ViewGroup.MarginLayoutParams){

            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)layoutParams ;

            marginLayoutParams.leftMargin *= scale;
            marginLayoutParams.rightMargin *= scale;
            marginLayoutParams.topMargin *=  scale;
            marginLayoutParams.bottomMargin *= scale;
        }

        //Set the layout information back into hte view

        rootView.setLayoutParams(layoutParams);

        //Scale the view's padding

        rootView.setPadding((int)(rootView.getPaddingLeft() *scale), (int)(rootView.getPaddingTop() *scale), (int)(rootView.getPaddingRight()*scale), (int) (rootView.getPaddingBottom() *scale));

        //If the root view is a TextView, scal the size of its text

        if (rootView instanceof TextView){

            TextView textView = (TextView)rootView;
            textView.setTextSize(textView.getTextSize() *scale);
        }

        //If root view is a ViewGroup, scale all of its children recursively

        if (rootView instanceof ViewGroup){

            ViewGroup viewGroup = (ViewGroup)rootView;
            for (int cnt = 0; cnt < viewGroup.getChildCount(); ++cnt)
                scaleViewAndChildren(viewGroup.getChildAt(cnt), scale);
        }
    }
}

package com.joseph.TBD.main;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.adapter.DetailsViewPagerAdapter;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.joseph.TBD.commons.Constants.KEY_INDEX;
import static com.joseph.TBD.commons.Constants.KEY_NAME;
import static com.joseph.TBD.commons.Constants.KEY_POSITION;
import static com.joseph.TBD.commons.Constants.ligamentList;
import static com.joseph.TBD.commons.Constants.jointList;
import static com.joseph.TBD.commons.Constants.muscleAssociated;
import static com.joseph.TBD.commons.Constants.muscleDifferential;
import static com.joseph.TBD.commons.Constants.muscleList;
import static com.joseph.TBD.commons.Constants.nervesList;
import static com.joseph.TBD.commons.Constants.neuralList;
import static com.joseph.TBD.commons.Constants.organsList;
import static com.joseph.TBD.commons.Constants.periostealCauseTenision;
import static com.joseph.TBD.commons.Constants.periostealDifferential;
import static com.joseph.TBD.commons.Constants.periostealList;

public class DetailsListActivity extends CommonActivity{

    @BindView(R.id.tabs_details) TabLayout tabLayout ;
    @BindView(R.id.viewpager) ViewPager viewPager;
    DetailsViewPagerAdapter adapter;
    @BindView(R.id.imv_back) ImageView ui_imvBack;

    @BindView(R.id.txv_title) TextView ui_txvTitle;

    public int index = 0;
    public int _position = 0;
    public String name = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_list);

        index = getIntent().getIntExtra(KEY_INDEX, 0);
        _position = getIntent().getIntExtra(KEY_POSITION, 0);
        name = getIntent().getStringExtra(KEY_NAME);
        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout() {

        Log.d("DetailsActiPostion==>", String.valueOf(_position));

        FragmentManager manager = getSupportFragmentManager();
        adapter = new DetailsViewPagerAdapter(manager, this);
        viewPager = (ViewPager)findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(1);
        tabLayout.setupWithViewPager(viewPager);



        if (name != null && !name.equals("null")){
            ui_txvTitle.setText(name);

        } else {
              /*Muscle */
            if (Constants.select_Status == 1) {
                ui_txvTitle.setText(muscleList.get(_position));
                /* Periosteal */
            } else if (Constants.select_Status == 4) {

                ui_txvTitle.setText(periostealList.get(_position));
            /*Ligament*/
            } else if (Constants.select_Status == 2) {

                ui_txvTitle.setText(ligamentList.get(_position));

            /*Joint*/
            } else if (Constants.select_Status == 3) {

                ui_txvTitle.setText(jointList.get(_position));
            }
            //Nerves
            else if (Constants.select_Status == 5) {

                ui_txvTitle.setText(nervesList.get(_position));
            }
            //Organs
            else if (Constants.select_Status == 6) {

                ui_txvTitle.setText(organsList.get(_position));
            }
            //Neural Matrix
            else if (Constants.select_Status == 7) {

                ui_txvTitle.setText(neuralList.get(_position));
            }
        }

    }

    @OnClick(R.id.imv_back) void gotoBack(){
        finish();
    }

}

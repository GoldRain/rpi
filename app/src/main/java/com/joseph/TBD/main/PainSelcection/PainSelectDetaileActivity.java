package com.joseph.TBD.main.PainSelcection;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.adapter.StoreAdapter;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.DetailsListActivity;
import com.joseph.TBD.model.ItemModel;
import com.joseph.TBD.model.DetailsModel;

import java.util.ArrayList;

import static com.joseph.TBD.commons.Constants.KEY_INDEX;
import static com.joseph.TBD.commons.Constants.KEY_NAME;
import static com.joseph.TBD.commons.Constants.KEY_POSITION;
import static com.joseph.TBD.commons.Constants.array_imageLigament;
import static com.joseph.TBD.commons.Constants.array_imageMuscle;
import static com.joseph.TBD.commons.Constants.array_imagePeriosteal1;
import static com.joseph.TBD.commons.Constants.imageName_index;
import static com.joseph.TBD.commons.Constants.joint_image;
import static com.joseph.TBD.commons.Constants.ligamentList;
import static com.joseph.TBD.commons.Constants.jointList;
import static com.joseph.TBD.commons.Constants.muscleList;
import static com.joseph.TBD.commons.Constants.nervesImages;
import static com.joseph.TBD.commons.Constants.nervesList;
import static com.joseph.TBD.commons.Constants.neuralImages;
import static com.joseph.TBD.commons.Constants.neuralList;
import static com.joseph.TBD.commons.Constants.organsImages;
import static com.joseph.TBD.commons.Constants.organsList;
import static com.joseph.TBD.commons.Constants.periostealList;
import static com.joseph.TBD.commons.Constants.region_name_name;
import static com.joseph.TBD.commons.Constants.region_name_region;
import static com.joseph.TBD.commons.Constants.screenheight;
import static com.joseph.TBD.commons.Constants.screenwidth;
import static com.joseph.TBD.commons.Constants.select_Status;

public class PainSelectDetaileActivity extends CommonActivity implements View.OnClickListener {

    TextView ui_txvCaption;
    ImageView ui_imvBack;
    GridView gridView;
    StoreAdapter storeAdapter;

    ArrayList<String> nameList = new ArrayList<>();

    LinearLayout lyt_title_pain_details;
    int titlbar = 0;
    int width = 0;
    int height = 0;

    ArrayList<DetailsModel> _detailsInfo = new ArrayList<>();

    ArrayList<Integer> _regionArray = new ArrayList<>();
    ArrayList<Integer> _indexRegion = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_painselectdetaile);


        _regionArray = getIntent().getIntegerArrayListExtra(Constants.KEY_REGION_ARRAY);

        for (int i = 0; i < _regionArray.size(); i++) {

            for (int j = 0; j < region_name_region.length; j++) {

                if (_regionArray.get(i) == region_name_region[j]) {

                    String temp = region_name_name[j];
                    Boolean flag = false;
                    for (int p = 0; p < nameList.size(); p++) {
                        if (temp.equals(nameList.get(p))) {
                            flag = true;
                        }
                    }

                    if (!flag) {
                        _indexRegion.add(imageName_index[j]);
                        nameList.add(region_name_name[j]);
                    }
                }
            }

        }

        for (int i = 0; i < nameList.size(); i++) {
            DetailsModel model = new DetailsModel();
            model.set_name(nameList.get(i));
        }

        loadLayout();

    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void getImageData(ArrayList<String> nameList) {

        _detailsInfo.clear();

        boolean isNext;
        for (int a = 0; a < nameList.size(); a++) {

            isNext = false;
            for (int b = 0; b < muscleList.size(); b++) {

                if (isNext) break;

                if (nameList.get(a).equals(muscleList.get(b))) {

                    DetailsModel _name = new DetailsModel();
                    _name.set_imageResource(array_imageMuscle.get(b));
                    _name.set_status(1);
                    _name.set_name(nameList.get(a));
                    _name.set_position(b);

                    isNext = true;
                    _detailsInfo.add(_name);
                    break;
                }
            }
            for (int d = 0; d < ligamentList.size(); d++) {

                if (isNext) break;

                if (nameList.get(a).equals(ligamentList.get(d))) {

                    DetailsModel _name = new DetailsModel();
                    _name.set_imageResource(array_imageLigament.get(d));
                    _name.set_status(2);
                    _name.set_name(nameList.get(a));
                    _name.set_position(d);

                    _detailsInfo.add(_name);

                    isNext = true;
                    break;
                }
            }

            for (int e = 0; e < jointList.size(); e++) {
                if (isNext) break;
                if (nameList.get(a).equals(jointList.get(e))) {

                    DetailsModel _name = new DetailsModel();
                    _name.set_imageResource(joint_image.get(e));
                    _name.set_status(3);
                    _name.set_name(nameList.get(a));
                    _name.set_position(e);

                    _detailsInfo.add(_name);
                    isNext = true;
                    break;
                }
            }

            for (int f = 0; f < periostealList.size(); f++) {
                if (isNext) break;
                if (nameList.get(a).equals(periostealList.get(f))) {

                    DetailsModel _name = new DetailsModel();
                    _name.set_imageResource(array_imagePeriosteal1.get(f));
                    _name.set_status(4);
                    _name.set_name(nameList.get(a));
                    _name.set_position(f);

                    _detailsInfo.add(_name);
                    isNext = true;
                    break;
                }
            }

            for (int g = 0; g < nervesList.size(); g++) {
                if (isNext) break;
                if (nameList.get(a).equals(nervesList.get(g))) {

                    DetailsModel _name = new DetailsModel();
                    _name.set_imageResource(nervesImages.get(g));
                    _name.set_status(5);
                    _name.set_name(nameList.get(a));
                    _name.set_position(g);

                    _detailsInfo.add(_name);
                    isNext = true;
                    break;
                }
            }

            for (int h = 0; h < organsList.size(); h++) {


                if (isNext) break;
                if (nameList.get(a).equals(organsList.get(h))) {

                    DetailsModel _name = new DetailsModel();
                    _name.set_imageResource(organsImages.get(h));
                    _name.set_status(6);
                    _name.set_name(nameList.get(a));
                    _name.set_position(h);

                    _detailsInfo.add(_name);
                    isNext = true;
                    break;
                }
            }

            for (int i = 0; i < neuralList.size(); i++) {
                if (isNext) break;
                if (nameList.get(a).equals(neuralList.get(i))) {

                    DetailsModel _name = new DetailsModel();
                    _name.set_imageResource(neuralImages.get(i));
                    _name.set_status(7);
                    _name.set_name(nameList.get(a));
                    _name.set_position(i);

                    _detailsInfo.add(_name);
                    isNext = true;
                    break;
                }
            }
        }
    }

    public void loadLayout(){

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        Display display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        final int statusbarheight = getStatusBarHeight();


        gridView = (GridView)findViewById(R.id.gridView);
        storeAdapter = new StoreAdapter(this);

        lyt_title_pain_details = (LinearLayout)findViewById(R.id.lyt_title_pain_details);

        lyt_title_pain_details.post(new Runnable()
        {

            @Override
            public void run()
            {

                titlbar = lyt_title_pain_details.getHeight();

                screenwidth = width - 5 * 3;
                screenheight = height - statusbarheight - titlbar - 15*3 - (35 + 27)*3 /* - 3 * 72*/;
                gridView.setAdapter(storeAdapter);

            }
        });


        ui_txvCaption = (TextView)findViewById(R.id.txv_title);

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);



        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DetailsModel selected = _detailsInfo.get(position);

                Constants.select_Status = selected.get_status();
                Constants.position = selected.get_position();

                Intent intent = new Intent(PainSelectDetaileActivity.this, DetailsListActivity.class);
                intent.putExtra(KEY_POSITION, Constants.position);
                intent.putExtra(KEY_INDEX, _indexRegion.get(position));
                intent.putExtra(KEY_NAME, nameList.get(position));
                startActivity(intent);
            }
        });

        getImageData(nameList);
        storeAdapter.setUserDatas(_detailsInfo);
        storeAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;

        }

    }
}

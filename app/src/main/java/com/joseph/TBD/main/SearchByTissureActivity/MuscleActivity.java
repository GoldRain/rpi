package com.joseph.TBD.main.SearchByTissureActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.joseph.TBD.R;
import com.joseph.TBD.adapter.CommonListAdapter;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.main.DetailsActivity.MuscleDetailsActivity;
import com.joseph.TBD.main.DetailsListActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.joseph.TBD.commons.Constants.KEY_IMAGE_VALUE;
import static com.joseph.TBD.commons.Constants.KEY_INDEX;
import static com.joseph.TBD.commons.Constants.KEY_NAME;
import static com.joseph.TBD.commons.Constants.KEY_POSITION;
import static com.joseph.TBD.commons.Constants.KEY_STRINGS;
import static com.joseph.TBD.commons.Constants.KEY_STRING_LISTS;
import static com.joseph.TBD.commons.Constants.KEY_TITLE;

public class MuscleActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack ,ui_imvCacel ;
    EditText ui_edtSearch;
    TextView txvTitle;

    ListView ui_lstContainer ;
    CommonListAdapter _adapter_mus;

    String[] strings = {};
    int[] imageValues = {};
    ArrayList<String> stringLists = new ArrayList<>();
    ArrayList<Integer> imageLists = new ArrayList<>();

    String _title = "";

    HashMap<String , Integer> hashValues = new HashMap<>();
    HashMap<String, Integer> hashImages = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muscle);

        //from TissuesFragment
        strings = getIntent().getStringArrayExtra(KEY_STRINGS);
        imageValues = getIntent().getIntArrayExtra(KEY_IMAGE_VALUE);
        stringLists = getIntent().getStringArrayListExtra(KEY_STRING_LISTS);
        _title = getIntent().getStringExtra(KEY_TITLE);

        for(int i = 0; i < strings.length; i ++) {
            hashValues.put(strings[i], imageValues[i]);

        }
        for (int j = 0; j < stringLists.size(); j++){
            hashImages.put(stringLists.get(j), j);
        }

        ButterKnife.bind(this);
        loadLayout();


    }

    private void loadLayout() {

        txvTitle = (TextView)findViewById(R.id.txv_title_com);

        if (_title.length() > 0)
            txvTitle.setText(_title);

        View header = (View)getLayoutInflater().inflate(R.layout.header_listview, null);
        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);
        ui_edtSearch = (EditText)findViewById(R.id.edt_search);
        ui_imvCacel = (ImageView)findViewById(R.id.imv_cancel);
        ui_imvCacel.setOnClickListener(this);

        _adapter_mus = new CommonListAdapter(this, stringLists);
        ui_lstContainer = (ListView)findViewById(R.id.lst_contaner_mus1);
        ui_lstContainer.addHeaderView(header);

        ui_edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (ui_edtSearch.getText().length()>0){
                    ui_imvCacel.setVisibility(View.VISIBLE);
                }else {
                    ui_imvCacel.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                String name = ui_edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                _adapter_mus.filter(name);

            }
        });

        _adapter_mus.addItem(stringLists);
        _adapter_mus.initProducts();
        _adapter_mus.notifyDataSetChanged();

        ui_lstContainer.setAdapter(_adapter_mus);

        ui_lstContainer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int positionAdd, long id) {

                int position = positionAdd -1;

                String name = stringLists.get(position);


                String name_position = _adapter_mus._contents.get(position);

                if(name.length() != 1 && !name_position.equals("Peripheral Nerves") && !name_position.equals("Spinal Nerves")) {

                    int imageIndex = hashValues.get(name);
                    int _position = hashImages.get(name);

                    //Constants.position = position;
                    Intent intent = new Intent(MuscleActivity.this, DetailsListActivity.class);
                    intent.putExtra(KEY_INDEX, imageIndex);
                    intent.putExtra(KEY_NAME, name_position);
                    intent.putExtra(KEY_POSITION, _position);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;

            case R.id.imv_cancel:
                ui_edtSearch.setText("");
                break;
        }

    }

}

package com.joseph.TBD.main;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.joseph.TBD.R;
import com.joseph.TBD.base.CommonActivity;
import com.joseph.TBD.commons.Constants;
import com.joseph.TBD.fragment.EulaFragment;
import com.joseph.TBD.fragment.HomeFragment;
import com.joseph.TBD.fragment.SymptomCheckerFragment;
import com.joseph.TBD.fragment.TissuesFragment;
import com.joseph.TBD.fragment.GlossaryFragment;
import com.joseph.TBD.fragment.TutorialFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.joseph.TBD.commons.Constants.anatomyImages;
import static com.joseph.TBD.commons.Constants.array_imageMuscle;
import static com.joseph.TBD.commons.Constants.array_imageLigament;
import static com.joseph.TBD.commons.Constants.array_imagePeriosteal1;
import static com.joseph.TBD.commons.Constants.ligamentList;
import static com.joseph.TBD.commons.Constants.jointList;
import static com.joseph.TBD.commons.Constants.joint_image;
import static com.joseph.TBD.commons.Constants.nervesImages;
import static com.joseph.TBD.commons.Constants.nervesList;
import static com.joseph.TBD.commons.Constants.neuralImages;
import static com.joseph.TBD.commons.Constants.neuralList;
import static com.joseph.TBD.commons.Constants.organsImages;
import static com.joseph.TBD.commons.Constants.organsList;
import static com.joseph.TBD.commons.Constants.periostealList;
import static com.joseph.TBD.commons.Constants.symptomCheckerList;

public class MainActivity extends CommonActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    ImageView ui_imvDrawer;
    ImageView ui_imvTreatment,ui_imvSearch, ui_imvLogo ;
    TextView ui_txvTreatment, ui_txvSearch;

    NavigationView ui_drawerMenu;
    DrawerLayout ui_drawerLayout;
    ActionBarDrawerToggle drawerToggle ;
    @BindView(R.id.imv_help) ImageView imv_help;

    LinearLayout ui_homeFrag, ui_searchFrag, ui_empty, ui_treatFrag;

    View headerView ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        loadLayout();
        setupNavigationBar();
    }

    private void loadLayout() {

        imv_help.setVisibility(View.VISIBLE);

        ui_drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);

        ui_imvDrawer = (ImageView)findViewById(R.id.imv_drawer);
        ui_imvDrawer.setOnClickListener(this);

        ui_imvLogo = (ImageView)findViewById(R.id.imv_logo);

        ui_imvSearch = (ImageView)findViewById(R.id.imv_search);
        ui_txvSearch = (TextView)findViewById(R.id.txv_title_search);

        ui_imvTreatment = (ImageView)findViewById(R.id.imv_treatment);
        ui_txvTreatment = (TextView)findViewById(R.id.txv_title_treatment);

        ui_drawerMenu = (NavigationView)findViewById(R.id.drawer_menu);
        ui_drawerMenu.setNavigationItemSelectedListener(this);
        ui_drawerLayout.addDrawerListener(drawerToggle);

        if(ligamentList.size() < 1){

            ligamentList.add("A");
            ligamentList.add("Acromioclavicular");
            ligamentList.add("Anterior and Posterior Cruciate");
            ligamentList.add("C");
            ligamentList.add("Cervical Facet Capsule (Lower)");
            ligamentList.add("Cervical Facet Capsule (Mid)");
            ligamentList.add("Cervical Facet Capsule (Upper)");
            ligamentList.add("Costosternal");
            ligamentList.add("Costotransverse");
            ligamentList.add("D");
            ligamentList.add("Deltoid");
            ligamentList.add("Dorsal Carpal");
            ligamentList.add("E");
            ligamentList.add("Extensor Retinaculum (Ankle)");
            ligamentList.add("F");
            ligamentList.add("Femoralacetabular Joint Capsule");
            ligamentList.add("G");
            ligamentList.add("Glenohumeral Joint Capsule");
            ligamentList.add("I");
            ligamentList.add("Iliolumbar");
            ligamentList.add("Inguinal");
            ligamentList.add("Interosseous Membrane (Forearm)");
            ligamentList.add("Interosseous Membrane (Shin)");
            ligamentList.add("L");
            ligamentList.add("Lateral Collateral (Knee)");
            ligamentList.add("Lumbar Facet Capsule");
            ligamentList.add("M");
            ligamentList.add("Medial Collateral (Knee)");
            ligamentList.add("P");
            ligamentList.add("Palmer Carpal");
            ligamentList.add("Pubic Symphysis");
            ligamentList.add("R");
            ligamentList.add("Radial Collateral");
            ligamentList.add("S");
            ligamentList.add("Sacroiliac");
            ligamentList.add("Sacrospinous");
            ligamentList.add("Sacrotuberous");
            ligamentList.add("Sternoclavicular");
            ligamentList.add("Supraspinous (C5)");
            ligamentList.add("Supraspinous (C6)");
            ligamentList.add("Supraspinous (C7)");
            ligamentList.add("Supraspinous (L3)");
            ligamentList.add("Supraspinous (L4)");
            ligamentList.add("Supraspinous (L5)");
            ligamentList.add("Supraspinous (S1)");
            ligamentList.add("Supraspinous (T1)");
            ligamentList.add("T");
            ligamentList.add("Talofibular");
            ligamentList.add("Thoracic Facet Capsule (Upper)");
            ligamentList.add("Thoracolumbar Facet Capsule");
            ligamentList.add("U");
            ligamentList.add("Ulnar Collateral");
        }

        if (symptomCheckerList.size() < 1){

            symptomCheckerList.add("A");
            symptomCheckerList.add("Abdominal Pain");
            symptomCheckerList.add("Ankle Pain ");
            symptomCheckerList.add("Anterior Neck Pain");
            symptomCheckerList.add("Anterior Rib Pain");
            symptomCheckerList.add("Anterior Shoulder Pain");
            symptomCheckerList.add("Anterior Thigh Pain");

            symptomCheckerList.add("B");
            symptomCheckerList.add("Buttock Pain ");

            symptomCheckerList.add("C");
            symptomCheckerList.add("Calf Pain");
            symptomCheckerList.add("Central Pelvic Pain");
            symptomCheckerList.add("Cervico-Thoracic Junction Pain");
            symptomCheckerList.add("Chest Pain");

            symptomCheckerList.add("E");
            symptomCheckerList.add("Ear Pain");
            symptomCheckerList.add("Elbow Pain");
            symptomCheckerList.add("Eye Pain");

            symptomCheckerList.add("F");
            symptomCheckerList.add("Finger Pain");
            symptomCheckerList.add("Flank Pain");
            symptomCheckerList.add("Forearm Pain");

            symptomCheckerList.add("G");
            symptomCheckerList.add("Groin Pain");

            symptomCheckerList.add("H");
            symptomCheckerList.add("Hand Pain");
            symptomCheckerList.add("Headache");
            symptomCheckerList.add("Heel Pain");

            symptomCheckerList.add("J");
            symptomCheckerList.add("Jaw Pain");

            symptomCheckerList.add("K");
            symptomCheckerList.add("Knee Pain");

            symptomCheckerList.add("L");
            symptomCheckerList.add("Lateral Hip Pain");
            symptomCheckerList.add("Lateral Neck Pain");
            symptomCheckerList.add("Lower Back Pain");

            symptomCheckerList.add("M");
            symptomCheckerList.add("Mid Thoracic Pain");

            symptomCheckerList.add("P");
            symptomCheckerList.add("Plantar Foot Pain");
            symptomCheckerList.add("Posterior Neck Pain");
            symptomCheckerList.add("Posterior Shoulder Pain");
            symptomCheckerList.add("Posterior Thigh Pain");

            symptomCheckerList.add("S");
            symptomCheckerList.add("Sacral Pain");
            symptomCheckerList.add("Sacroiliac Pain");
            symptomCheckerList.add("Scapular Pain");
            symptomCheckerList.add("Shin Pain");
            symptomCheckerList.add("Spinal Pain");

            symptomCheckerList.add("T");
            symptomCheckerList.add("Thumb Pain");
            symptomCheckerList.add("Toe Pain");

            symptomCheckerList.add("U");
            symptomCheckerList.add("Upper Arm Pain");

            symptomCheckerList.add("W");
            symptomCheckerList.add("Wrist Pain");



        }

        if(periostealList.size() < 1){

            periostealList.add("A");
            periostealList.add("Angle of the Ribs");
            periostealList.add("C");
            periostealList.add("C2 Spinous Process");
            periostealList.add("Calcaneal Spur");
            periostealList.add("Clavicle (Sternal End)");
            periostealList.add("Coccyx");
            periostealList.add("D");
            periostealList.add("Deltoid Insertion");
            periostealList.add("E");
            periostealList.add("Erb's Point");
            periostealList.add("F");
            periostealList.add("Fibular Head");
            periostealList.add("I");
            periostealList.add("Iliac Crest");
            periostealList.add("Ischial Tuberosity");
            periostealList.add("K");
            periostealList.add("Knee Joint Line");
            periostealList.add("L");
            periostealList.add("Lateral Epicondyle");
            periostealList.add("Lateral Pubic Symphysis");
            periostealList.add("M");
            periostealList.add("Mandibular Condyle");
            periostealList.add("Medial Epicondyle");
            periostealList.add("Metatarsal Head");
            periostealList.add("N");
            periostealList.add("Nuchal Line");
            periostealList.add("P");
            periostealList.add("Pes Anserine");
            periostealList.add("Posterior Foramen Magnum");
            periostealList.add("R");
            periostealList.add("Radial Styloid");
            periostealList.add("Ribs (Anterior)");
            periostealList.add("Ribs (Axilla)");
            periostealList.add("S");
            periostealList.add("Spinous Process");
            periostealList.add("Sternalcostal (1st Rib)");
            periostealList.add("Sternoclavicular");
            periostealList.add("Superior Patella");
            periostealList.add("Superior Pubic Symphysis");
            periostealList.add("T");
            periostealList.add("Transverse Process of Atlas");
            periostealList.add("X");
            periostealList.add("Xiphoid");
        }

        if(Constants.muscleList.size() < 1) {
            Constants.muscleList.add("A");
            Constants.muscleList.add("Abductor Digiti Minimi (foot)");
            Constants.muscleList.add("Abductor Digiti Minimi (hand)");
            Constants.muscleList.add("Abductor Hallucis");
            Constants.muscleList.add("Adductor Hallicis");
            Constants.muscleList.add("Adductor Longus and Brevis");
            Constants.muscleList.add("Adductor Magnus");
            Constants.muscleList.add("Adductor Pollicis");
            Constants.muscleList.add("Anconeus");
            Constants.muscleList.add("Anterior Deltoid");
            Constants.muscleList.add("B");
            Constants.muscleList.add("Biceps Brachii");
            Constants.muscleList.add("Biceps Femoris");
            Constants.muscleList.add("Brachialis");
            Constants.muscleList.add("Brachioradialis");
            Constants.muscleList.add("Buccinator");
            Constants.muscleList.add("C");
            Constants.muscleList.add("Coracobrachialis");
            Constants.muscleList.add("D");
            Constants.muscleList.add("Diagastric 1");
            Constants.muscleList.add("Diagastric 2");
            Constants.muscleList.add("Diaphragm");
            Constants.muscleList.add("E");
            Constants.muscleList.add("Extensor Carpi Radialis Brevis");
            Constants.muscleList.add("Extensor Carpi Radialis Longus");
            Constants.muscleList.add("Extensor Carpi Ulnaris");
            Constants.muscleList.add("Extensor Digitorum 1");
            Constants.muscleList.add("Extensor Digitorum 2");
            Constants.muscleList.add("Extensor Digitorum Brevis");
            Constants.muscleList.add("Extensor Digitorum Longus");
            Constants.muscleList.add("Extensor Hallucis Brevis");
            Constants.muscleList.add("Extensor Hallucis Longus");
            Constants.muscleList.add("Extensor Indicis");
            Constants.muscleList.add("External Oblique");
            Constants.muscleList.add("F");
            Constants.muscleList.add("First Dorsal Interosseous of the Foot");
            Constants.muscleList.add("First Dorsal Interosseous of the Hand");
            Constants.muscleList.add("Flexor Carpi Radialis");
            Constants.muscleList.add("Flexor Carpi Ulnaris");
            Constants.muscleList.add("Flexor Digitorum Brevis");
            Constants.muscleList.add("Flexor Digitorum Longus");
            Constants.muscleList.add("Flexor Digitorum Profundus");
            Constants.muscleList.add("Flexor Digitorum Superficialis");
            Constants.muscleList.add("Flexor Hallucis Brevis");
            Constants.muscleList.add("Flexor Hallucis Longus");
            Constants.muscleList.add("Flexor Pollicis Longus");
            Constants.muscleList.add("G");
            Constants.muscleList.add("Gastrocnemius 1");
            Constants.muscleList.add("Gastrocnemius 2");
            Constants.muscleList.add("Gastrocnemius 3");
            Constants.muscleList.add("Gastrocnemius 4");
            Constants.muscleList.add("Gluteus Maximus 1");
            Constants.muscleList.add("Gluteus Maximus 2");
            Constants.muscleList.add("Gluteus Maximus 3");
            Constants.muscleList.add("Gluteus Medius 1");
            Constants.muscleList.add("Gluteus Medius 2");
            Constants.muscleList.add("Gluteus Medius 3");
            Constants.muscleList.add("Gluteus Minimus 1");
            Constants.muscleList.add("Gluteus Minimus 2");
            Constants.muscleList.add("Gracilis");
            Constants.muscleList.add("I");
            Constants.muscleList.add("Iliopsoas");
            Constants.muscleList.add("Infraspinatus");
            Constants.muscleList.add("Intercostal");
            Constants.muscleList.add("L");
            Constants.muscleList.add("Lateral Oblique");
            Constants.muscleList.add("Lateral Pterygoid");
            Constants.muscleList.add("Latissimus Dorsi 1");
            Constants.muscleList.add("Latissimus Dorsi 2");
            Constants.muscleList.add("Levator Scapulae");
            Constants.muscleList.add("M");
            Constants.muscleList.add("Masseter 1");
            Constants.muscleList.add("Masseter 2");
            Constants.muscleList.add("Masseter 3");
            Constants.muscleList.add("Masseter 4");
            Constants.muscleList.add("Medial Pterygoid");
            Constants.muscleList.add("Middle Deltoid");
            Constants.muscleList.add("Multifidus (Cervical)");
            Constants.muscleList.add("Multifidus (Thoracic and Lumbar)");
            Constants.muscleList.add("O");
            Constants.muscleList.add("Obturator Internus");
            Constants.muscleList.add("Occipitalis");
            Constants.muscleList.add("Occipitofrontalis");
            Constants.muscleList.add("Opponens Pollicis");
            Constants.muscleList.add("Orbicularis Oculi");
            Constants.muscleList.add("P");
            Constants.muscleList.add("Palmaris Longus");
            Constants.muscleList.add("Paraspinal (Mid Thoracic)");
            Constants.muscleList.add("Paraspinal (Thoracolumbar)");
            Constants.muscleList.add("Pectineus");
            Constants.muscleList.add("Pectoralis Major 1");
            Constants.muscleList.add("Pectoralis Major 2");
            Constants.muscleList.add("Pectoralis Major 3");
            Constants.muscleList.add("Pectoralis Major 4");
            Constants.muscleList.add("Pectoralis Minor");
            Constants.muscleList.add("Pelvic Floor");
            Constants.muscleList.add("Peroneus Brevis");
            Constants.muscleList.add("Peroneus Longus");
            Constants.muscleList.add("Peroneus Tertius");
            Constants.muscleList.add("Piriformis 1");
            Constants.muscleList.add("Piriformis 2");
            Constants.muscleList.add("Plantaris");
            Constants.muscleList.add("Platysma");
            Constants.muscleList.add("Popliteus");
            Constants.muscleList.add("Posterior Deltoid");
            Constants.muscleList.add("Pronator Teres");
            Constants.muscleList.add("Q");
            Constants.muscleList.add("Quadratus Lumborum 1");
            Constants.muscleList.add("Quadratus Lumborum 2");
            Constants.muscleList.add("Quadratus Lumborum 3");
            Constants.muscleList.add("Quadratus Lumborum 4");
            Constants.muscleList.add("Quadratus Plantae");
            Constants.muscleList.add("R");
            Constants.muscleList.add("Rectus Abdominis");
            Constants.muscleList.add("Rectus Femoris");
            Constants.muscleList.add("Rhomboid");
            Constants.muscleList.add("S");
            Constants.muscleList.add("Sartorius");
            Constants.muscleList.add("Scalene");
            Constants.muscleList.add("Scalenus Minimus");
            Constants.muscleList.add("Second Dorsal Interosseous of the Hand");
            Constants.muscleList.add("Semimembranosus and Semitendinosus");
            Constants.muscleList.add("Semispinalis Capitis 1");
            Constants.muscleList.add("Semispinalis Capitis 2");
            Constants.muscleList.add("Serratus Anterior");
            Constants.muscleList.add("Serratus Posterior Inferior");
            Constants.muscleList.add("Serratus Posterior Superior");
            Constants.muscleList.add("Soleus 1");
            Constants.muscleList.add("Soleus 2");
            Constants.muscleList.add("Soleus 3");
            Constants.muscleList.add("Soleus 4");
            Constants.muscleList.add("Splenius Cervicis 1");
            Constants.muscleList.add("Splenius Cervicis 2");
            Constants.muscleList.add("Sternalis");
            Constants.muscleList.add("Sternocleidomastoid 1");
            Constants.muscleList.add("Sternocleidomastoid 2");
            Constants.muscleList.add("Subclavius");
            Constants.muscleList.add("Suboccipital");
            Constants.muscleList.add("Subscapularis");
            Constants.muscleList.add("Supinator");
            Constants.muscleList.add("Supraspinatus 1");
            Constants.muscleList.add("Supraspinatus 2");
            Constants.muscleList.add("T");
            Constants.muscleList.add("Temporalis 1");
            Constants.muscleList.add("Temporalis 2");
            Constants.muscleList.add("Temporalis 3");
            Constants.muscleList.add("Temporalis 4");
            Constants.muscleList.add("Tensor Fasciae Latae");
            Constants.muscleList.add("Teres Major");
            Constants.muscleList.add("Teres Minor");
            Constants.muscleList.add("Tibialis Anterior");
            Constants.muscleList.add("Tibialis Posterior");
            Constants.muscleList.add("Trapezius 1");
            Constants.muscleList.add("Trapezius 2");
            Constants.muscleList.add("Trapezius 3");
            Constants.muscleList.add("Trapezius 4");
            Constants.muscleList.add("Trapezius 5");
            Constants.muscleList.add("Trapezius 6");
            Constants.muscleList.add("Trapezius 7");
            Constants.muscleList.add("Triceps Brachii 1");
            Constants.muscleList.add("Triceps Brachii 2");
            Constants.muscleList.add("Triceps Brachii 3");
            Constants.muscleList.add("Triceps Brachii 4");
            Constants.muscleList.add("Triceps Brachii 5");
            Constants.muscleList.add("V");
            Constants.muscleList.add("Vastus Intermedius");
            Constants.muscleList.add("Vastus Lateralis 1");
            Constants.muscleList.add("Vastus Lateralis 2");
            Constants.muscleList.add("Vastus Lateralis 3");
            Constants.muscleList.add("Vastus Lateralis 4");
            Constants.muscleList.add("Vastus Lateralis 5");
            Constants.muscleList.add("Vastus Medialis 1");
            Constants.muscleList.add("Vastus Medialis 2");
            Constants.muscleList.add("Z");
            Constants.muscleList.add("Zygomaticus Major");
        }

        if(jointList.size() < 1){

            jointList.add("D");
            jointList.add("Dura (Cervicothoracic)");
            jointList.add("Dura (Thoracic)");
            jointList.add("Dura (Thoracolumbar)");
            jointList.add("F");
            jointList.add("Facet C0-C1-C2");
            jointList.add("Facet C2-C3");
            jointList.add("Facet C3-C4");
            jointList.add("Facet C4-C5");
            jointList.add("Facet C5-C6");
            jointList.add("Facet C6-C7");
            jointList.add("Facet C7-T1");
            jointList.add("Facet Lumbar");
            jointList.add("Facet T1-T2");
            jointList.add("Facet T11-T12");
            jointList.add("Facet T3-T4");
            jointList.add("Facet T5-T6");
            jointList.add("Facet T6-T7");
            jointList.add("Facet T7-T11");
            jointList.add("FacetT2-T3");
            jointList.add("FacetT4-T5");
            jointList.add("S");
            jointList.add("Sacroiliac Joint");
            jointList.add("Sclerotome C1");
            jointList.add("Sclerotome C2");
            jointList.add("Sclerotome C3");
            jointList.add("Sclerotome C4");
            jointList.add("Sclerotome C5");
            jointList.add("Sclerotome C6");
            jointList.add("Sclerotome C7");
            jointList.add("Sclerotome C8");
            jointList.add("Sclerotome L1");
            jointList.add("Sclerotome L2");
            jointList.add("Sclerotome L3");
            jointList.add("Sclerotome L4");
            jointList.add("Sclerotome L5");
            jointList.add("Sclerotome S1");
            jointList.add("Sclerotome T1");
            jointList.add("Sclerotome T10");
            jointList.add("Sclerotome T11");
            jointList.add("Sclerotome T12");
            jointList.add("Sclerotome T2");
            jointList.add("Sclerotome T3");
            jointList.add("Sclerotome T4");
            jointList.add("Sclerotome T5");
            jointList.add("Sclerotome T6");
            jointList.add("Sclerotome T7");
            jointList.add("Sclerotome T8");
            jointList.add("Sclerotome T9");
            jointList.add("T");
            jointList.add("Thermatome (T1-T2)");
            jointList.add("Thermatome (T1-T4)");
            jointList.add("Thermatome (T11-L2)");
            jointList.add("Thermatome (T2-T7)");
            jointList.add("Thermatome (T4-L2)");
        }

        if (nervesList.size() < 1){

            nervesList.add("Peripheral Nerves");

            nervesList.add("Anterior Femoral Cutaneous L2-L3");
            nervesList.add("Axillary Superior Lateral Cutaneous C5-C6");
            nervesList.add("Common Peroneal Lateral Sural Cutaneous L4-S2");
            nervesList.add("Deep Peroneal L4-L5");
            nervesList.add("Ilioinguinal L1");
            nervesList.add("Intercostobrachial T2");
            nervesList.add("Last Thoracic Nerve T12");
            nervesList.add("Lateral Antebrachial Cutaneous C5-C6");
            nervesList.add("Lateral Cutaneous Branch of Iliohypogastric L1");
            nervesList.add("Lateral Femoral Cutaneous L2-L3");
            nervesList.add("Lumboinguinal L1-L2");
            nervesList.add("Medial Antebrachial Cutaneous C8-T1");
            nervesList.add("Medial Brachial Cutaneous T1-T2");
            nervesList.add("Median C5-C8");
            nervesList.add("Middle Cluneal");
            nervesList.add("Posterior Femoral Cutaneous S1-S3");
            nervesList.add("Radial Dorsal Antebrachial Cutaneous C5-C6");
            nervesList.add("Radial Superficial C6-C8");
            nervesList.add("Saphenous Medial Crural Cutaneous L3-L4");
            nervesList.add("Superior Peroneal L4-S1");
            nervesList.add("Superior Cluneal L1-L3");
            nervesList.add("Supraclavicular C3-C4");
            nervesList.add("Sural S1-S2");
            nervesList.add("Tibial Medial Calcaneal S1-S2");
            nervesList.add("Ulnar Palmer C8-T1");
            nervesList.add("Unlar Dorsal C8-T1");

            nervesList.add("Spinal Nerves");

            nervesList.add("C2");
            nervesList.add("C3");
            nervesList.add("C4");
            nervesList.add("C5");
            nervesList.add("C6");
            nervesList.add("C7");
            nervesList.add("C8");

            nervesList.add("L1");
            nervesList.add("L2");
            nervesList.add("L3");
            nervesList.add("L4");
            nervesList.add("L5");

            nervesList.add("S1");
            nervesList.add("S2");
            nervesList.add("S3");
            nervesList.add("S4");
            nervesList.add("S5");

            nervesList.add("T1");
            nervesList.add("T2");
            nervesList.add("T3");
            nervesList.add("T4");
            nervesList.add("T5");
            nervesList.add("T6");
            nervesList.add("T7");
            nervesList.add("T8");
            nervesList.add("T9");
            nervesList.add("T10");
            nervesList.add("T11");
            nervesList.add("T12");
        }

        if (nervesImages.size() < 1){

            nervesImages.add(0);

            nervesImages.add(R.mipmap.nerv_anterior_femoral_cutaneousl2_l3);
            nervesImages.add(R.mipmap.nerv_axillary_superior_lateral_cutaneous_c5_c6);
            nervesImages.add(R.mipmap.nerv_common_peroneal_lateral_sural_cutaneous_l4_s2);
            nervesImages.add(R.mipmap.nerv_deep_deroneal_l4_l5);
            nervesImages.add(R.mipmap.nerv_ilioinguinal_l1);
            nervesImages.add(R.mipmap.nerv_intercostobrachial_t2);
            nervesImages.add(R.mipmap.nerv_last_thoracic_nerve_t12);
            nervesImages.add(R.mipmap.nerv_lateral_antebrachial_cutaneous_c5_c6);
            nervesImages.add(R.mipmap.nerv_lateral_cutaneous_branch_iliohypo_astric_l1);
            nervesImages.add(R.mipmap.nerv_lateral_femoral_cutaneous_l2_l3);
            nervesImages.add(R.mipmap.nerv_lumboinguinal_l2);
            nervesImages.add(R.mipmap.nerv_medial_antebrachial_cutaneous_c8_t1);
            nervesImages.add(R.mipmap.nerv_medial_brachial_cutaneous_t1_t2);
            nervesImages.add(R.mipmap.nerv_median_c5_c8);
            nervesImages.add(R.mipmap.nerv_middle_cluneal);
            nervesImages.add(R.mipmap.nerv_posterior_femoral_cutaneous_s1_s3);
            nervesImages.add(R.mipmap.nerv_radial_dorsal_antebrachial_cutaneousc5_c6);
            nervesImages.add(R.mipmap.nerv_radial_superficial_c6_c8);
            nervesImages.add(R.mipmap.nerv_saphenous_medial_crural_cutaneous_l3_l4);
            nervesImages.add(R.mipmap.nerv_superior_peroneal_l4_s1);
            nervesImages.add(R.mipmap.nerv_supperior_cluneal_l1_l3);
            nervesImages.add(R.mipmap.nerv_supraclavicular_c3_c4);
            nervesImages.add(R.mipmap.nerv_sural_s1_s2);
            nervesImages.add(R.mipmap.nerv_tibial_medial_calcaneal_s1_s2);
            nervesImages.add(R.mipmap.nerv_ulnar_palmer_c8_t1);
            nervesImages.add(R.mipmap.nerv_unlard_dorsal_c8_t1);

            nervesImages.add(0);
            nervesImages.add(R.mipmap.nerv_c2);
            nervesImages.add(R.mipmap.nerv_c3);
            nervesImages.add(R.mipmap.nerv_c4);
            nervesImages.add(R.mipmap.nerv_c5);
            nervesImages.add(R.mipmap.nerv_c6);
            nervesImages.add(R.mipmap.nerv_c7);
            nervesImages.add(R.mipmap.nerv_c8);

            nervesImages.add(R.mipmap.nerv_l1);
            nervesImages.add(R.mipmap.nerv_l2);
            nervesImages.add(R.mipmap.nerv_l3);
            nervesImages.add(R.mipmap.nerv_l4);
            nervesImages.add(R.mipmap.nerv_l5);

            nervesImages.add(R.mipmap.nerv_s1);
            nervesImages.add(R.mipmap.nerv_s2);
            nervesImages.add(R.mipmap.nerv_s3);
            nervesImages.add(R.mipmap.nerv_s4);
            nervesImages.add(R.mipmap.nerv_s5);

            nervesImages.add(R.mipmap.nerv_t1);
            nervesImages.add(R.mipmap.nerv_t2);
            nervesImages.add(R.mipmap.nerv_t3);
            nervesImages.add(R.mipmap.nerv_t4);
            nervesImages.add(R.mipmap.nerv_t5);
            nervesImages.add(R.mipmap.nerv_t6);
            nervesImages.add(R.mipmap.nerv_t7);
            nervesImages.add(R.mipmap.nerv_t8);
            nervesImages.add(R.mipmap.nerv_t9);
            nervesImages.add(R.mipmap.nerv_t10);
            nervesImages.add(R.mipmap.nerv_t11);
            nervesImages.add(R.mipmap.nerv_t12);
        }

        if (organsList.size() < 1){

            organsList.add("A");
            organsList.add("Appendix");
            organsList.add("C");
            organsList.add("Colon");
            organsList.add("E");
            organsList.add("Esophagus");
            organsList.add("H");
            organsList.add("Heart");
            organsList.add("K");
            organsList.add("Kidney");
            organsList.add("L");
            organsList.add("Larynx");
            organsList.add("Liver Gall Bladder");
            organsList.add("Lung Diaphragm");
            organsList.add("O");
            organsList.add("Ovary");
            organsList.add("P");
            organsList.add("Pancreas");
            organsList.add("Prostate");
            organsList.add("S");
            organsList.add("Small Intestine");
            organsList.add("Spleen");
            organsList.add("Stomach");
            organsList.add("T");
            organsList.add("Testies");
            organsList.add("U");
            organsList.add("Urinary Bladder");
            organsList.add("Uterus Seminal Vesicles");

        }

        if (organsImages.size() < 1){

            organsImages.add(0);

            organsImages.add(R.mipmap.organs_appendix);

            organsImages.add(0);
            organsImages.add(R.mipmap.organs_colon);

            organsImages.add(0);
            organsImages.add(R.mipmap.organs_esophagus);

            organsImages.add(0);
            organsImages.add(R.mipmap.organs_heart);

            organsImages.add(0);
            organsImages.add(R.mipmap.organs_kidney);

            organsImages.add(0);
            organsImages.add(R.mipmap.organs_larynx);
            organsImages.add(R.mipmap.organs_liver_gallbladder);
            organsImages.add(R.mipmap.organs_lung_diaphragm);

            organsImages.add(0);
            organsImages.add(R.mipmap.organs_ovary);

            organsImages.add(0);
            organsImages.add(R.mipmap.organs_pancreas);
            organsImages.add(R.mipmap.organs_prostate);

            organsImages.add(0);
            organsImages.add(R.mipmap.organs_small_intestines);
            organsImages.add(R.mipmap.organs_spleen);
            organsImages.add(R.mipmap.organs_stomach);

            organsImages.add(0);
            organsImages.add(R.mipmap.organs_testies);

            organsImages.add(0);
            organsImages.add(R.mipmap.organs_urinary_bladder);
            organsImages.add(R.mipmap.organs_uterus_seminal_vesicles);

        }

        if (neuralList.size() < 1){

            neuralList.add("E");
            neuralList.add("Elbow Pain");
            neuralList.add("F");
            neuralList.add("Foot and Ankle Pain");
            neuralList.add("H");
            neuralList.add("Hand and Wrist Pain");
            neuralList.add("Hip Pain");
            neuralList.add("K");
            neuralList.add("Knee Pain");
            neuralList.add("L");
            neuralList.add("Low Back Pain");
            neuralList.add("N");
            neuralList.add("Neck Pain");
            neuralList.add("S");
            neuralList.add("Shoulder Pain");

        }

        if (neuralImages.size() < 1){

            neuralImages.add(0);
            neuralImages.add(R.mipmap.neural_elbow_pain);

            neuralImages.add(0);
            neuralImages.add(R.mipmap.neural_foot_ankle_pain);

            neuralImages.add(0);
            neuralImages.add(R.mipmap.neural_hand_wrist_pain);
            neuralImages.add(R.mipmap.neural_hip_pain);

            neuralImages.add(0);
            neuralImages.add(R.mipmap.neural_knee_pain);

            neuralImages.add(0);
            neuralImages.add(R.mipmap.neural_low_back_pain);

            neuralImages.add(0);
            neuralImages.add(R.mipmap.neural_neck_pain);

            neuralImages.add(0);
            neuralImages.add(R.mipmap.neural_shoulder_pain);



        }

        //images muscle
        if(array_imageMuscle.size() < 1) {
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.abductordigitiminimi_foot_s);
            array_imageMuscle.add(R.mipmap.abductordigitiminimi_hand_s);
            array_imageMuscle.add(R.mipmap.abductorhallucis_s);
            array_imageMuscle.add(R.mipmap.adductorhallicis_s);
            array_imageMuscle.add(R.mipmap.adductorlongusandbrevis_s);
            array_imageMuscle.add(R.mipmap.adductormagnus_s);
            array_imageMuscle.add(R.mipmap.adductorpollicis_s);
            array_imageMuscle.add(R.mipmap.anconeus_s);
            array_imageMuscle.add(R.mipmap.anteriordeltoid_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.bicepsbrachii_s);
            array_imageMuscle.add(R.mipmap.bicepsfemoris_s);
            array_imageMuscle.add(R.mipmap.brachialis_s);
            array_imageMuscle.add(R.mipmap.brachioradialis_s);
            array_imageMuscle.add(R.mipmap.buccinator_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.coracobrachialis_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.diagastric1_s);
            array_imageMuscle.add(R.mipmap.diagastric2_s);
            array_imageMuscle.add(R.mipmap.diaphragm_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.extensorcarpiradialisbrevis_s);
            array_imageMuscle.add(R.mipmap.extensorcarpiradialislongus_s);
            array_imageMuscle.add(R.mipmap.extensorcarpiulnaris_s);
            array_imageMuscle.add(R.mipmap.extensordigitorum1_s);
            array_imageMuscle.add(R.mipmap.extensordigitorum2_s);
            array_imageMuscle.add(R.mipmap.extensordigitorumbrevis_s);
            array_imageMuscle.add(R.mipmap.extensordigitorumlongus_s);
            array_imageMuscle.add(R.mipmap.extensorhallucisbrevis_s);
            array_imageMuscle.add(R.mipmap.extensorhallucislongus_s);
            array_imageMuscle.add(R.mipmap.extensorindicis_s);
            array_imageMuscle.add(R.mipmap.externaloblique_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.firstdorsalinterosseousofthefoot_s);
            array_imageMuscle.add(R.mipmap.firstdorsalinterosseousofthehand_s);
            array_imageMuscle.add(R.mipmap.flexorcarpiradialis_s);
            array_imageMuscle.add(R.mipmap.flexorcarpiulnaris_s);
            array_imageMuscle.add(R.mipmap.flexordigitorumbrevis_s);
            array_imageMuscle.add(R.mipmap.flexordigitorumlongus_s);
            array_imageMuscle.add(R.mipmap.flexordigitorumprofundus_s);
            array_imageMuscle.add(R.mipmap.flexordigitorumsuperficialis_s);
            array_imageMuscle.add(R.mipmap.flexorhallucisbrevis_s);
            array_imageMuscle.add(R.mipmap.flexorhallucislongus_s);
            array_imageMuscle.add(R.mipmap.flexorpollicislongus_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.gastrocnemius1_s);
            array_imageMuscle.add(R.mipmap.gastrocnemius2_s);
            array_imageMuscle.add(R.mipmap.gastrocnemius3_s);
            array_imageMuscle.add(R.mipmap.gastrocnemius4_s);
            array_imageMuscle.add(R.mipmap.gluteusmaximus1_s);
            array_imageMuscle.add(R.mipmap.gluteusmaximus2_s);
            array_imageMuscle.add(R.mipmap.gluteusmaximus3_s);
            array_imageMuscle.add(R.mipmap.gluteusmedius1_s);
            array_imageMuscle.add(R.mipmap.gluteusmedius2_s);
            array_imageMuscle.add(R.mipmap.gluteusmedius3_s);
            array_imageMuscle.add(R.mipmap.gluteusminimus1_s);
            array_imageMuscle.add(R.mipmap.gluteusminimus2_s);
            array_imageMuscle.add(R.mipmap.gracilis_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.iliopsoas_s);
            array_imageMuscle.add(R.mipmap.infraspinatus_s);
            array_imageMuscle.add(R.mipmap.intercostal_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.lateraloblique_s);
            array_imageMuscle.add(R.mipmap.lateralpterygoid_s);
            array_imageMuscle.add(R.mipmap.latissimusdorsi1_s);
            array_imageMuscle.add(R.mipmap.latissimusdorsi2_s);
            array_imageMuscle.add(R.mipmap.levatorscapulae_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.masseter1_s);
            array_imageMuscle.add(R.mipmap.masseter2_s);
            array_imageMuscle.add(R.mipmap.masseter3_s);
            array_imageMuscle.add(R.mipmap.masseter4_s);
            array_imageMuscle.add(R.mipmap.medialpterygoid_s);
            array_imageMuscle.add(R.mipmap.middledeltoid_s);
            array_imageMuscle.add(R.mipmap.multifidus_cervical_s);
            array_imageMuscle.add(R.mipmap.multifidus_thoracicandlumbar_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.obturatorinternus_s);
            array_imageMuscle.add(R.mipmap.occipitalis_s);
            array_imageMuscle.add(R.mipmap.occipitofrontalis_s);
            array_imageMuscle.add(R.mipmap.opponenspollicis_s);
            array_imageMuscle.add(R.mipmap.orbicularisoculi_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.palmarislongus_s);
            array_imageMuscle.add(R.mipmap.paraspinal_midthoracic_s);
            array_imageMuscle.add(R.mipmap.paraspinal_thoracolumbar_s);
            array_imageMuscle.add(R.mipmap.pectineus_s);
            array_imageMuscle.add(R.mipmap.pectoralismajor1_s);
            array_imageMuscle.add(R.mipmap.pectoralismajor2_s);
            array_imageMuscle.add(R.mipmap.pectoralismajor3_s);
            array_imageMuscle.add(R.mipmap.pectoralismajor4_s);
            array_imageMuscle.add(R.mipmap.pectoralisminor_s);
            array_imageMuscle.add(R.mipmap.pelvicfloor_s);
            array_imageMuscle.add(R.mipmap.peroneusbrevis_s);
            array_imageMuscle.add(R.mipmap.peroneuslongus_s);
            array_imageMuscle.add(R.mipmap.peroneustertius_s);
            array_imageMuscle.add(R.mipmap.piriformis1_s);
            array_imageMuscle.add(R.mipmap.piriformis2_s);
            array_imageMuscle.add(R.mipmap.plantaris_s);
            array_imageMuscle.add(R.mipmap.platysma_s);
            array_imageMuscle.add(R.mipmap.popliteus_s);
            array_imageMuscle.add(R.mipmap.posteriordeltoid_s);
            array_imageMuscle.add(R.mipmap.pronatorteres_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.quadratuslumborum1_s);
            array_imageMuscle.add(R.mipmap.quadratuslumborum2_s);
            array_imageMuscle.add(R.mipmap.quadratuslumborum3_s);
            array_imageMuscle.add(R.mipmap.quadratuslumborum4_s);
            array_imageMuscle.add(R.mipmap.quadratusplantae_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.rectusabdominis_s);
            array_imageMuscle.add(R.mipmap.rectusfemoris_s);
            array_imageMuscle.add(R.mipmap.rhomboid_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.sartorius_s);
            array_imageMuscle.add(R.mipmap.scalene_s);
            array_imageMuscle.add(R.mipmap.scalenusminimus_s);
            array_imageMuscle.add(R.mipmap.seconddorsalinterosseousofthehand_s);
            array_imageMuscle.add(R.mipmap.semimembranosusandsemitendinosus_s);
            array_imageMuscle.add(R.mipmap.semispinaliscapitis1_s);
            array_imageMuscle.add(R.mipmap.semispinaliscapitis2_s);
            array_imageMuscle.add(R.mipmap.serratusanterior_s);
            array_imageMuscle.add(R.mipmap.serratusposteriorinferior_s);
            array_imageMuscle.add(R.mipmap.serratusposteriorsuperior_s);
            array_imageMuscle.add(R.mipmap.soleus1_s);
            array_imageMuscle.add(R.mipmap.soleus2_s);
            array_imageMuscle.add(R.mipmap.soleus3_s);
            array_imageMuscle.add(R.mipmap.soleus4_s);
            array_imageMuscle.add(R.mipmap.spleniuscervicis1_s);
            array_imageMuscle.add(R.mipmap.spleniuscervicis2_s);
            array_imageMuscle.add(R.mipmap.sternalis_s);
            array_imageMuscle.add(R.mipmap.sternocleidomastoid1_s);
            array_imageMuscle.add(R.mipmap.sternocleidomastoid2_s);
            array_imageMuscle.add(R.mipmap.subclavius_s);
            array_imageMuscle.add(R.mipmap.suboccipital_s);
            array_imageMuscle.add(R.mipmap.subscapularis_s);
            array_imageMuscle.add(R.mipmap.supinator_s);
            array_imageMuscle.add(R.mipmap.supraspinatus1_s);
            array_imageMuscle.add(R.mipmap.supraspinatus2_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.temporalis1_s);
            array_imageMuscle.add(R.mipmap.temporalis2_s);
            array_imageMuscle.add(R.mipmap.temporalis3_s);
            array_imageMuscle.add(R.mipmap.temporalis4_s);
            array_imageMuscle.add(R.mipmap.tensorfasciaelatae_s);
            array_imageMuscle.add(R.mipmap.teresmajor_s);
            array_imageMuscle.add(R.mipmap.teresminor_s);
            array_imageMuscle.add(R.mipmap.tibialisanterior_s);
            array_imageMuscle.add(R.mipmap.tibialisposterior_s);
            array_imageMuscle.add(R.mipmap.trapezius1_s);
            array_imageMuscle.add(R.mipmap.trapezius2_s);
            array_imageMuscle.add(R.mipmap.trapezius3_s);
            array_imageMuscle.add(R.mipmap.trapezius4_s);
            array_imageMuscle.add(R.mipmap.trapezius5_s);
            array_imageMuscle.add(R.mipmap.trapezius6_s);
            array_imageMuscle.add(R.mipmap.trapezius7_s);
            array_imageMuscle.add(R.mipmap.tricepsbrachii1_s);
            array_imageMuscle.add(R.mipmap.tricepsbrachii2_s);
            array_imageMuscle.add(R.mipmap.tricepsbrachii3_s);
            array_imageMuscle.add(R.mipmap.tricepsbrachii4_s);
            array_imageMuscle.add(R.mipmap.tricepsbrachii5_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.vastusintermedius_s);
            array_imageMuscle.add(R.mipmap.vastuslateralis1_s);
            array_imageMuscle.add(R.mipmap.vastuslateralis2_s);
            array_imageMuscle.add(R.mipmap.vastuslateralis3_s);
            array_imageMuscle.add(R.mipmap.vastuslateralis4_s);
            array_imageMuscle.add(R.mipmap.vastuslateralis5_s);
            array_imageMuscle.add(R.mipmap.vastusmedialis1_s);
            array_imageMuscle.add(R.mipmap.vastusmedialis2_s);
            array_imageMuscle.add(0);
            array_imageMuscle.add(R.mipmap.zygomaticusmajor_s);

        }

        if(array_imagePeriosteal1.size() < 1){

            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.angleoftheribs_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.c2spinousprocess_s);
            array_imagePeriosteal1.add(R.mipmap.calcanealspur_s);
            array_imagePeriosteal1.add(R.mipmap.clavicle_sternalend_s);
            array_imagePeriosteal1.add(R.mipmap.coccyx_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.deltoidinsertion_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.erbspoint_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.fibularhead_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.iliaccrest_s);
            array_imagePeriosteal1.add(R.mipmap.ischialtuberosity_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.kneejointline_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.lateralepicondyle_s);
            array_imagePeriosteal1.add(R.mipmap.lateralpubicsymphysis_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.mandibularcondyle_s);
            array_imagePeriosteal1.add(R.mipmap.medialepicondyle_s);
            array_imagePeriosteal1.add(R.mipmap.metatarsalhead_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.nuchalline_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.pesanserine_s);
            array_imagePeriosteal1.add(R.mipmap.posteriorforamenmagnum_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.radialstyloid_s);
            array_imagePeriosteal1.add(R.mipmap.ribs_anterior_s);
            array_imagePeriosteal1.add(R.mipmap.ribs_axilla_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.spinousprocess_s);
            array_imagePeriosteal1.add(R.mipmap.sternalcostal_1strib_s);
            array_imagePeriosteal1.add(R.mipmap.sternoclavicular_s);
            array_imagePeriosteal1.add(R.mipmap.superiorpatella_s);
            array_imagePeriosteal1.add(R.mipmap.superiorpubicsymphysis_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.transverseprocessofatlas_s);
            array_imagePeriosteal1.add(0);
            array_imagePeriosteal1.add(R.mipmap.xiphoid_s);
        }

        if(joint_image.size() < 1){

            joint_image.add(0);
            joint_image.add(R.mipmap.dura_cervicothoracic);
            joint_image.add(R.mipmap.dura_thoracic);
            joint_image.add(R.mipmap.dura_thoracolumbar);
            joint_image.add(0);
            joint_image.add(R.mipmap.facetc0_c1_c2);
            joint_image.add(R.mipmap.facetc2_c3);
            joint_image.add(R.mipmap.facetc3_c4);
            joint_image.add(R.mipmap.facetc4_c5);
            joint_image.add(R.mipmap.facetc5_c6);
            joint_image.add(R.mipmap.facetc6_c7);
            joint_image.add(R.mipmap.facetc7_t1);
            joint_image.add(R.mipmap.facetlumbar);
            joint_image.add(R.mipmap.facett1_t2);
            joint_image.add(R.mipmap.facett11_t12);
            joint_image.add(R.mipmap.facett3_t4);
            joint_image.add(R.mipmap.facett5_t6);
            joint_image.add(R.mipmap.facett6_t7);
            joint_image.add(R.mipmap.facett7_t11);
            joint_image.add(R.mipmap.facett2_t3);
            joint_image.add(R.mipmap.facett4_t5);
            joint_image.add(0);
            joint_image.add(R.mipmap.sacroiliacjoint);
            joint_image.add(R.mipmap.sclerotomec1);
            joint_image.add(R.mipmap.sclerotomec2);
            joint_image.add(R.mipmap.sclerotomec3);
            joint_image.add(R.mipmap.sclerotomec4);
            joint_image.add(R.mipmap.sclerotomec5);
            joint_image.add(R.mipmap.sclerotomec6);
            joint_image.add(R.mipmap.sclerotomec7);
            joint_image.add(R.mipmap.sclerotomec8);
            joint_image.add(R.mipmap.sclerotomel1);
            joint_image.add(R.mipmap.sclerotomel2);
            joint_image.add(R.mipmap.sclerotomel3);
            joint_image.add(R.mipmap.sclerotomel4);
            joint_image.add(R.mipmap.sclerotomel5);
            joint_image.add(R.mipmap.sclerotomes1);
            joint_image.add(R.mipmap.sclerotomet1);
            joint_image.add(R.mipmap.sclerotomet10);
            joint_image.add(R.mipmap.sclerotomet11);
            joint_image.add(R.mipmap.sclerotomet12);
            joint_image.add(R.mipmap.sclerotomet2);
            joint_image.add(R.mipmap.sclerotomet3);
            joint_image.add(R.mipmap.sclerotomet4);
            joint_image.add(R.mipmap.sclerotomet5);
            joint_image.add(R.mipmap.sclerotomet6);
            joint_image.add(R.mipmap.sclerotomet7);
            joint_image.add(R.mipmap.sclerotomet8);
            joint_image.add(R.mipmap.sclerotomet9);
            joint_image.add(0);
            joint_image.add(R.mipmap.thermatome_t1_t2);
            joint_image.add(R.mipmap.thermatome_t1_t4);
            joint_image.add(R.mipmap.thermatome_t11_l2);
            joint_image.add(R.mipmap.thermatome_t2_t7);
            joint_image.add(R.mipmap.thermatome_t4_l2);
        }

        if(array_imageLigament.size() < 1){
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.acromioclavicular);
            array_imageLigament.add(R.drawable.anteriorandposteriorcruciate);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.cervicalfacetcapsule_lower);
            array_imageLigament.add(R.drawable.cervicalfacet_capsule_mid);
            array_imageLigament.add(R.drawable.cervicalfacetcapsule_upper);
            array_imageLigament.add(R.drawable.costosternal);
            array_imageLigament.add(R.drawable.costotransverse);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.deltoid);
            array_imageLigament.add(R.drawable.dorsalcarpal);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.extensorretinaculum_ankle);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.femoralacetabularjointcapsule);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.glenohumeraljointcapsule);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.iliolumbar);
            array_imageLigament.add(R.drawable.inguinal);
            array_imageLigament.add(R.drawable.interosseousmembrane_forearm);
            array_imageLigament.add(R.drawable.interosseousmembrane_shin);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.lateralcollateral_knee);
            array_imageLigament.add(R.drawable.lumbarfacetcapsule);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.medialcollateral_knee);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.palmercarpal);
            array_imageLigament.add(R.drawable.pubicsymphysis);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.radialcollateral);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.sacroiliac);
            array_imageLigament.add(R.drawable.sacrospinous);
            array_imageLigament.add(R.drawable.sacrotuberous);
            array_imageLigament.add(R.drawable.sternoclavicular);
            array_imageLigament.add(R.drawable.supraspinatus_c5);
            array_imageLigament.add(R.drawable.supraspinatus_c6);
            array_imageLigament.add(R.drawable.supraspinatus_c7);
            array_imageLigament.add(R.drawable.supraspinatus_l3);
            array_imageLigament.add(R.drawable.supraspinatus_l4);
            array_imageLigament.add(R.drawable.supraspinatus_l5);
            array_imageLigament.add(R.drawable.supraspinatus_s1);
            array_imageLigament.add(R.drawable.supraspinatus_t1);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.talofibular);
            array_imageLigament.add(R.drawable.thoracicfacetcapsule_upper);
            array_imageLigament.add(R.drawable.thoracolumbarfacetcapsule);
            array_imageLigament.add(0);
            array_imageLigament.add(R.drawable.ulnarcollateral);
        }
        //Anatomy images
        if (anatomyImages.size() < 1){

            anatomyImages.add(R.mipmap.anatomy_peripheral_nerves);
            anatomyImages.add(R.mipmap.anatomy_slide1);
            anatomyImages.add(R.mipmap.anatomy_slide2);
            anatomyImages.add(R.mipmap.anatomy_slide3);
            anatomyImages.add(R.mipmap.anatomy_slide4);
            anatomyImages.add(R.mipmap.anatomy_slide5);
            anatomyImages.add(R.mipmap.anatomy_slide6);
            anatomyImages.add(R.mipmap.anatomy_slide7);
            anatomyImages.add(R.mipmap.anatomy_slide8);
            anatomyImages.add(R.mipmap.anatomy_slide9);
            anatomyImages.add(R.mipmap.anatomy_slide10);
            anatomyImages.add(R.mipmap.anatomy_slide11);
            anatomyImages.add(R.mipmap.anatomy_slide12);
            anatomyImages.add(R.mipmap.anatomy_slide13);
            anatomyImages.add(R.mipmap.anatomy_slide14);
            anatomyImages.add(R.mipmap.anatomy_slide15);
            anatomyImages.add(R.mipmap.anatomy_slide16);
            anatomyImages.add(R.mipmap.anatomy_slide17);
            anatomyImages.add(R.mipmap.anatomy_slide18);
            anatomyImages.add(R.mipmap.anatomy_slide19);
            anatomyImages.add(R.mipmap.anatomy_slide20);
            anatomyImages.add(R.mipmap.anatomy_slide21);
            anatomyImages.add(R.mipmap.anatomy_slide22);
            anatomyImages.add(R.mipmap.anatomy_slide23);
            anatomyImages.add(R.mipmap.anatomy_slide24);
            anatomyImages.add(R.mipmap.anatomy_slide25);
            anatomyImages.add(R.mipmap.anatomy_slide26);
            anatomyImages.add(R.mipmap.anatomy_slide27);
            anatomyImages.add(R.mipmap.anatomy_slide28);
            anatomyImages.add(R.mipmap.anatomy_slide29);
            anatomyImages.add(R.mipmap.anatomy_slide30);
            anatomyImages.add(R.mipmap.anatomy_slide31);
            anatomyImages.add(R.mipmap.anatomy_slide32);
            anatomyImages.add(R.mipmap.anatomy_slide33);
            anatomyImages.add(R.mipmap.anatomy_slide34);
            anatomyImages.add(R.mipmap.anatomy_slide35);
            anatomyImages.add(R.mipmap.anatomy_slide36);
            anatomyImages.add(R.mipmap.anatomy_spinal_nerves);
        }


       /* select HomeTitle */
        titleChcange(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE);
        if(Constants.activitystatus==0){
            gotoHomeFragment();
        }else if(Constants.activitystatus==1){
            gotoTissueFragment();
        }



    }


    private void setupNavigationBar() {

        drawerToggle = new ActionBarDrawerToggle(this, ui_drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);}

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);}
        };

        ui_drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void showDrawer() {

        ui_drawerLayout.openDrawer(Gravity.LEFT);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_drawer:
                showDrawer();
                break;

        }

    }

    @Override
    public boolean onNavigationItemSelected( MenuItem item) {

        int id = item.getItemId();

        if (item.isChecked()) item.setChecked(false); else item.setChecked(true);
        ui_drawerLayout.closeDrawers();

        switch (id){

            case R.id.nav_home:
                gotoHomeFragment();
                break;

            case R.id.nav_symptom:
                gotoSymptomCheckerFragment();
                break;

            case R.id.nav_tissues:
                gotoTissueFragment();
                break;

        }

        return true;
    }



    public void gotoTissueFragment() {
        imv_help.setVisibility(View.GONE);

        TissuesFragment fragment = new TissuesFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();

    }

    public void gotoSymptomCheckerFragment() {

        imv_help.setVisibility(View.GONE);

        SymptomCheckerFragment fragment = new SymptomCheckerFragment(/*this*/);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();

    }

    public void gotoGlossaryFragment(){

        imv_help.setVisibility(View.VISIBLE);
        GlossaryFragment fragment = new GlossaryFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoEulaFragment(){

        imv_help.setVisibility(View.VISIBLE);
        EulaFragment fragment = new EulaFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoTutorialFragment(){
        imv_help.setVisibility(View.VISIBLE);

        TutorialFragment fragment = new TutorialFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoHomeFragment(){
        imv_help.setVisibility(View.VISIBLE);

        HomeFragment fragment = new HomeFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

  /*  public void gotoTerms(){

        startActivity(new Intent(this, OrientationActivity.class));

        overridePendingTransition(0,0);

    }*/

    public void titleChcange(int a, int b, int c,int d, int e, int f){

        ui_imvLogo.setVisibility(a);

        ui_imvSearch.setVisibility(b);
        ui_txvSearch.setVisibility(c);

        ui_imvTreatment.setVisibility(d);
        ui_txvTreatment.setVisibility(e);

        //imv_help.setVisibility(f);

    }

    @Override
    public void onBackPressed() {

        if (ui_drawerLayout.isDrawerOpen(GravityCompat.START)){
            this.ui_drawerLayout.closeDrawer(GravityCompat.START);

        } else {

            onExit();
        }
    }

    @OnClick(R.id.imv_help) void showMenu(){

        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(MainActivity.this, imv_help);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.pop_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

           /* case R.id.tb_setting:
                return true;*/
                    case R.id.popup_glossary:

                        gotoGlossaryFragment();
                        return true;

                    case R.id.popup_tutorial:
                        gotoTutorialFragment();
                        return true;

                    case R.id.popup_eula:
                        gotoEulaFragment();
                        return true;
                }

                Toast.makeText(MainActivity.this,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                return true;

            }
        });

        popup.show();//showing popup menu
    }


}

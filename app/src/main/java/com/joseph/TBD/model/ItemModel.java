package com.joseph.TBD.model;

import java.io.Serializable;

/**
 * Created by ToSuccess on 11/23/2016.
 */

public class ItemModel implements Serializable{

    int position = 0;
    int select_status = 0;

    public void setPosition(int position){
        this.position = position;
    }

    public int getPosition(){
        return this.position;
    }

    public void setSelect_status(int select_status){
        this.select_status = select_status;
    }

    public int getSelect_status(){
        return this.select_status;
    }
}

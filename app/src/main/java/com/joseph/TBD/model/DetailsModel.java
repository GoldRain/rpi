package com.joseph.TBD.model;

import java.io.Serializable;

/**
 * Created by ITWhiz4U on 1/16/2018.
 */

public class DetailsModel implements Serializable {

    int _status = 0;
    String _name = "";
    int _imageResource = 0;
    int _position = 0;

    public int get_status() {
        return _status;
    }

    public void set_status(int _position) {
        this._status = _position;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public int get_imageResource() {
        return _imageResource;
    }

    public void set_imageResource(int _imageResource) {
        this._imageResource = _imageResource;
    }

    public int get_position() {
        return _position;
    }

    public void set_position(int _position) {
        this._position = _position;
    }
}
